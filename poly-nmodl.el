(require 'polymode)
(defcustom pm-host/nmodl
  (pm-host-chunkmode :name "nmodl"
                     :mode 'nmodl-mode)
  "NMODL host chunkmode"
  :group 'poly-hostmodes
  :type 'object)

(defcustom  pm-inner/verbatim-c-code
  (pm-inner-chunkmode :name "verbatim-c-code"
                      :mode 'c-mode
                      :adjust-face 8
                      :head-matcher "^[ \t]*VERBATIM[ \t]*[\n\r]?"
                      :tail-matcher "^[ \t]*ENDVERBATIM$"
                      :head-mode 'host
                      :tail-mode 'host)
  "VERBATIM C code block."
  :group 'poly-innermodes
  :type 'object)

(define-polymode poly-nmodl-mode
  :hostmode 'pm-host/nmodl
  :innermodes '(pm-inner/verbatim-c-code))

(provide 'poly-nmodl)
